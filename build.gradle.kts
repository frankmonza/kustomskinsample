buildscript {
    repositories {
        mavenCentral()
        google()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:7.3.1")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.5.21")
    }
}

allprojects {
    allprojects {
        repositories {
            mavenCentral()
            google()
        }
        ext {
            set("buildToolsVersion", "33.0.0")
            set("compileSdkVersion", 33)
            set("targetSdkVersion", 33)
        }
    }
}
