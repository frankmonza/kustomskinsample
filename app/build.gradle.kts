plugins {
    id("com.android.application")
    kotlin("android")
}

android {
    compileSdkVersion(rootProject.ext.get("compileSdkVersion") as Int)
    buildToolsVersion = rootProject.ext.get("buildToolsVersion") as String

    defaultConfig {

        /**
         * CHANGE THIS TO YOUR APP PKG NAME THIS CANNOT BE CHANGED AFTER FIRST RELEASE
         */
        applicationId = "whatever.kompany.skin"

        /**
         * INCREMENT THIS IF YOU WANT TO RELEASE UPDATES
         */
        versionCode = 1
        versionName = "1"

        minSdkVersion(21)
        targetSdkVersion(rootProject.ext.get("targetSdkVersion") as Int)
    }

    buildTypes {
        release {
            isShrinkResources = false
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules")
        }
        aaptOptions {
            noCompress("zip", "komp", "klwp", "kwgt", "klck", "kwch", "kntf")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    namespace = "whatever.kompany.skin"
}

dependencies {
    implementation("org.bitbucket.frankmonza:kustomapi:1.3")
}
